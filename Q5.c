#include <stdio.h>
#include <stdlib.h>

int main()
{
   int j, i, n;
   printf("Enter an integer : ");
   scanf("%d",&n);
   printf("Multiplication tables from 1 to %d \n",n);
   for(i=1;i<=10;i++)
   {
     for(j=1;j<=n;j++)
     {
       if (j<=n-1)
           printf("%d * %d = %d       ",j,i,i*j);
          else
	    printf("%d * %d = %d          ",j,i,i*j);

      }
     printf("\n");
    }
    return 0;
}
